## Day in the life of a Data Scientist - Mani Sarkar
_(aka From backend development to machine learning)_

### Slides

See [slides (PDF)](Backend Developer to ML Engineer_Data Science.pdf) - please download to be able to click on the links (see top corner for Download button when on the page with the PDF displayed). `Linux` or `Windows` users in case links don't work, try to open the PDF in a web browser and it should work.

### Video

See [recording](https://drive.google.com/file/d/1XKoXoP9Sud8043L5sh7NcLSay0YSxylV/view)

### Notebook

See [Jupyter notebook](./simple-nyc-taxi-trip-duration-analysis.ipynb)

#### Pre-requisites
- Python 3.7 or higher
- Install packages via the `requirements.txt`
- Cloned git repo (this repo)
- Downloaded dataset (see below for link)
- Place zip file into the root folder of repo

#### Getting started

- Clone repository
```bash
$ git clone git@gitlab.com:aicoder2022/datascience-ml-talks.git
$ code datascience-ml-talks/2022-08-30
```

- Download the [New York Taxi Trip duration (Extended) datasets](https://drive.google.com/file/d/1Bp9T6BKghr20exHMqRaMH6S6__QG_s6m/view?usp=sharing) and place the zip file in the `datascience-ml-talks/2022-08-30` folder.

- Follow these steps:
```bash
$ unzip nyc-taxi-trip-datasets.zip
$ pip install -r requirements.txt
```
On successful installation of the python packages, perform the next step.

- Run Jupyter labs while inside the project folder
```bash
$ jupyter lab .
```

This should open the notebook inside the default browser.

Note: `$` is the command-prompt you get on your macOS or linux machines.

### Speaker

- [Mani Sarkar](http://github.com/neomatrix369)

### Abstract

In my journey to better myself, learn newer and more interesting subjects and technologies I have found that we often don't get access to all the tools or learning resources in the right order. So I went ahead and created my own order or list and then my own tools - both of which you can see via these two GitHub repos: https://github.com/neomatrix369/awesome-ai-ml-dl and https://bit.ly/better-nlp-launch.

Followed by a case-study of a fictitious tours and travel company called "Wonders of New York" - an exploratory data analysis based on the New York Taxi Trip duration dataset and a stakeholder's question to dig out useful insights to make business decision(s).